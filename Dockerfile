FROM golang:1.17-alpine

WORKDIR /app

COPY go.mod ./
RUN go mod download

COPY . .

RUN go build -o ./bin/data-engine

EXPOSE 31234

CMD ["./bin/data-engine", "--filePath", "test-data/filestore"]