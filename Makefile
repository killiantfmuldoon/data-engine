build:
	go build -o bin/data-engine main.go

test:
	go test -p 1 ./...

image:
	docker build . -t kmuldoon/data-engine
