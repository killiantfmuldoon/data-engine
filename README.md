# Data engine

Data engine serves an API that consumes json messages representing trades, stores them and keeps track of positions in specific instruments accessible by ID and by timestamp.

## Getting started

### Prerequisites
This guide is written to use Docker to run the program and curl to interact with the API. Alternatively the program can be run as a standalone Go binary and accessed using an API testing tool such as Postman.

### Running data-engine
Data-engine can be run as a go binary or using Docker. To build and run the image:

```bash
make image && docker run -p 31234:31234 kmuldoon/data-engine:latest
```

This will build and start the image and sets it to listen on port 31234.

### Reading positions
The docker image is built with a file of test data that it reads on start up. Positions should be readable for `instrument_id`s from 1-9. After starting the Docker container this should be available in the browser by going to (for example)
`http://localhost:31234/api/v1/position/3/current`


To see the most up-to-date position information use:
```bash
curl localhost:31234/api/v1/position/{POSITION_ID}/current
```

Position data can also be retrieved for a specified timestamp using:
```bash
curl localhost:31234/api/v1/position/{POSITION_ID}/{TIMESTAMP}
```

### Creating a new trade
Trades have the format:
```json
{

         "instrument_id": 4,

         "trade_size": 100,

         "trade_price": 23.0,

         "trade_timestamp": 1650896178

}
```

To create a new trade using the docker endpoint using curl use:

```bash
curl -X POST -H "Content-Type: application/json" -d '{

         "instrument_id": 4,

         "trade_size": 150,

         "trade_price": 7.0,

         "trade_timestamp": 1650896182

}'  localhost:31234/api/v1/trade/
```
