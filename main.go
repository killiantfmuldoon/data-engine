package main

import (
	"data-engine/pkg/server"
	"data-engine/pkg/store"
	"flag"
	"log"
	"os"
)

func main() {
	fileStorePath := flag.String(
		"filePath",
		"",
		"Path to a file which can be used as a persistent data store. If nil no persistent storage is used.")

	port := flag.Int(
		"port",
		31234,
		"Port on which the server will listen.")

	flag.Parse()
	// Create the file if it does not exist.
	if len(*fileStorePath) > 0 {
		_, err := os.OpenFile(*fileStorePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Fatal(err)
		}
	}

	fileStore := &store.FileStore{Filename: *fileStorePath}

	// Initialize the cache from the fileStore if persistent information exists.
	cache := store.InitializeCache(fileStore)
	server.Start(&store.CacheWithPersistentStore{
		Persistent: fileStore,
		Cache:      cache,
	}, *port)
}
