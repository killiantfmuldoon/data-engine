// Package v1 contains information about the types handled by the data engine
package v1

import (
	"encoding/json"
	"fmt"
	"io"
)

const (
	Prefix = "/api/v1"
)

// Trade contains information about individual trades.
type Trade struct {
	InstrumentID int64   `json:"instrument_id"`
	Size         int64   `json:"trade_size"`
	Price        float64 `json:"trade_price"`
	Timestamp    int64   `json:"trade_timestamp"`
}

// Position describes the size of a specific instrument held at a point in time.
type Position struct {
	InstrumentID int64 `json:"instrument_id"`
	Size         int64 `json:"position_size"`
	Timestamp    int64 `json:"position_timestamp"`
}

// ToJSONString takes returns a string representation of the TradeStruct.
func (t *Trade) ToJSONString() (string, error) {
	bytes, err := json.Marshal(t)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

// FromJSON takes an io.Reader with JSON data representing a Trade and decodes it to the receiving Trade struct.
func (t *Trade) FromJSON(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(&t)
	if err != nil {
		return err
	}
	if !t.IsValid() {
		return fmt.Errorf("trade JSON values not valid %s", reader)
	}
	return nil
}

// IsValid does naive validation on the Trade.
func (t *Trade) IsValid() bool {
	return t.InstrumentID > 0 &&
		t.Price > 0 &&
		t.Timestamp > 0
}

// IsValid does naive validation on the Position.
func (p Position) IsValid() bool {
	return p.InstrumentID > 0 &&
		p.Size > 0 &&
		p.Timestamp > 0
}
