package v1

import (
	"io"
	"strings"
	"testing"
)

func TestPosition_IsValid(t *testing.T) {
	tests := []struct {
		name     string
		position Position
		want     bool
	}{
		{
			name:     "succeed for correct Position",
			position: Position{InstrumentID: 1, Size: 2, Timestamp: 4},
			want:     true,
		},
		{
			name:     "fail for negative InstrumentID",
			position: Position{InstrumentID: -1, Size: 2, Timestamp: 4},
			want:     false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.position.IsValid(); got != tt.want {
				t.Errorf("IsValid() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTrade_FromJSON(t1 *testing.T) {
	tests := []struct {
		name    string
		json    io.Reader
		want    Trade
		wantErr bool
	}{
		{
			name:    "succeed with basic conversion",
			json:    strings.NewReader("{\"instrument_id\":1,\"trade_size\":2,\"trade_price\":3.999999999999,\"trade_timestamp\":4}"),
			want:    Trade{InstrumentID: 1, Size: 2, Price: 3.999999999999, Timestamp: 4},
			wantErr: false,
		},
		{
			name:    "fail with malformed json",
			json:    strings.NewReader("{:1,\"trade_size\":2,\"trade_price\":3.999999999999,\"trade_timestamp\":4}"),
			want:    Trade{InstrumentID: 1, Size: 2, Price: 3.999999999999, Timestamp: 4},
			wantErr: true,
		},
		{
			name:    "fail with wrong key",
			json:    strings.NewReader("{\"wrong_key\":1,\"trade_size\":2,\"trade_price\":3.999999999999,\"trade_timestamp\":4}"),
			want:    Trade{InstrumentID: 1, Size: 2, Price: 3.999999999999, Timestamp: 4},
			wantErr: true,
		},
		{
			name:    "fail with wrong value types",
			json:    strings.NewReader("{\"instrument_id\":1.9999,\"trade_size\":2,\"trade_price\":3.999999999999,\"trade_timestamp\":4}"),
			want:    Trade{InstrumentID: 1, Size: 2, Price: 3.999999999999, Timestamp: 4},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			got := &Trade{}
			if err := got.FromJSON(tt.json); (err != nil) != tt.wantErr {
				t1.Errorf("FromJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !tt.wantErr && (*got != tt.want) {
				t1.Errorf("FromJSON() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTrade_IsValid(t1 *testing.T) {
	tests := []struct {
		name  string
		trade Trade
		want  bool
	}{
		{
			name:  "succeed with positive values",
			trade: Trade{InstrumentID: 1, Size: 2, Price: 3.999999999999, Timestamp: 4},
			want:  true,
		},
		{
			name:  "fail for negative timestamp",
			trade: Trade{InstrumentID: 1, Size: 2, Price: 3.999999999999, Timestamp: -4},
			want:  false,
		},
		{
			name:  "fail for negative price",
			trade: Trade{InstrumentID: 1, Size: 2, Price: -3.999999999999, Timestamp: 4},
			want:  false,
		},
		{
			name:  "succeed for negative size",
			trade: Trade{InstrumentID: 1, Size: -2, Price: 3.999999999999, Timestamp: 4},
			want:  true,
		},
		{
			name:  "fail for negative InstrumentID",
			trade: Trade{InstrumentID: -1, Size: 2, Price: 3.999999999999, Timestamp: 4},
			want:  false,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			if got := tt.trade.IsValid(); got != tt.want {
				t1.Errorf("IsValid() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTrade_ToJSONString(t1 *testing.T) {
	tests := []struct {
		name    string
		trade   *Trade
		want    string
		wantErr bool
	}{
		{
			name:    "succeed for int",
			trade:   &Trade{InstrumentID: 1, Size: 2, Price: 3, Timestamp: 4},
			want:    "{\"instrument_id\":1,\"trade_size\":2,\"trade_price\":3,\"trade_timestamp\":4}",
			wantErr: false,
		},
		{
			name:    "succeed for floats",
			trade:   &Trade{InstrumentID: 1, Size: 2, Price: 3.999999999999, Timestamp: 4},
			want:    "{\"instrument_id\":1,\"trade_size\":2,\"trade_price\":3.999999999999,\"trade_timestamp\":4}",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			got, err := tt.trade.ToJSONString()
			if (err != nil) != tt.wantErr {
				t1.Errorf("ToJSONString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t1.Errorf("ToJSONString() got = %v, want %v", got, tt.want)
			}
		})
	}
}
