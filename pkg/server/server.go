// Package server contains a server implementation used to read positions and write trades.
package server

import (
	v1 "data-engine/pkg/api/v1"
	"data-engine/pkg/store"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"path"
	"strings"
)

type server struct {
	*http.Server
	client store.Client
}

// Start initializes the Server and adds the required handlers.
func Start(dataStore store.Client, port int) {
	srv := &server{
		&http.Server{Addr: ":" + fmt.Sprintf("%d", port)},
		dataStore,
	}

	mx := http.NewServeMux()
	mx.HandleFunc(v1.Prefix+"/position/", srv.positionHandler)
	mx.HandleFunc(v1.Prefix+"/trade/", srv.tradeHandler)
	srv.Handler = mx
	log.Fatal(srv.ListenAndServe())
}

// tradeHandler validates the Trade request and writes it to the data store.
func (s *server) tradeHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	trade := &v1.Trade{}

	if r.Body == nil {
		log.Print("trade request body can not be empty")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Here we decode the request and write it to the datastore.
	if err := trade.FromJSON(r.Body); err != nil {
		log.Print("trade request body could not be decoded")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if err := r.Body.Close(); err != nil {
		log.Print("trade request body could not be closed")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// For trades only POST is supported.
	switch r.Method {
	case http.MethodPost:
		if err := s.client.WriteTrade(*trade); err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
}

// positionHandler handles get requests for permissions by parsing the URL and returning the requested data from the
// client reader.
func (s *server) positionHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	instrumentID, timestamp, err := parsePositionURL(r.RequestURI)
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// For positions only GET is supported.
	switch r.Method {
	case http.MethodGet:
		position, err := s.client.ReadPosition(instrumentID, timestamp)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if err := json.NewEncoder(w).Encode(position); err != nil {
			log.Print(err)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			}
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
}

// parsePositionURL parses a request for a position.
// The correct form is: /api/v1/position/<POSITION_ID>/<TIMESTAMP>)
// <Timestamp> may also be "current" to get the most up-to-date data.
func parsePositionURL(urlString string) (string, string, error) {
	URL, err := url.Parse(urlString)
	if err != nil {
		return "", "", err
	}
	parts := strings.Split(path.Clean(URL.Path), "/")
	if len(parts) != 6 {
		err = fmt.Errorf("position path must be of the form %s", v1.Prefix+"/position/<POSITION_ID>/<TIMESTAMP")
		return "", "", err
	}

	timestamp := parts[len(parts)-1]
	instrumentID := parts[len(parts)-2]
	return instrumentID, timestamp, nil
}
