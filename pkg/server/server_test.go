package server

import (
	v1 "data-engine/pkg/api/v1"
	"data-engine/pkg/store"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"testing"
	"time"
)

// server tests are end to end style tests designed to get an understanding of the characteristics of the entire system.
func TestServer(t *testing.T) {
	testPort := 31235
	url := fmt.Sprintf("http://localhost:%d", testPort)

	// Create the file if it does not exist.
	_, err := os.OpenFile("/tmp/filestore", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	fileStore := &store.FileStore{Filename: "/tmp/filestore"}
	defer func() {
		err := os.Remove("/tmp/filestore")
		if err != nil {
			t.Error(err)
		}
	}()

	// Initialize the cache from the fileStore if persistent information exists.
	cache := store.InitializeCache(fileStore)
	go Start(&store.CacheWithPersistentStore{
		Persistent: fileStore,
		Cache:      cache,
	}, testPort)

	t.Run("Create a trade", func(t *testing.T) {
		resp, err := addTrade(url, 1, 2, 3.999999999999, 4)
		if err != nil {
			t.Errorf("error creating trade %s", err)
		}
		if resp.StatusCode != http.StatusOK {
			t.Errorf("error creating trade: %s", err)
		}
	})
	t.Run("Error creating a malformed trade", func(t *testing.T) {
		resp, _ := addTrade(url, 0, 2, 3.999999999999, 4)
		if resp.StatusCode == http.StatusOK {
			t.Errorf("trade should not have been created: %d", resp.StatusCode)
		}
	})
	t.Run("Read portfolio value from the api", func(t *testing.T) {
		position, err := checkPosition(url, 1, 4)
		if err != nil {
			t.Errorf("error getting current portfolio %s", err)
		}

		want := v1.Position{InstrumentID: 1, Size: 2, Timestamp: 4}
		if *position != want {
			t.Errorf("got = %v, want %v", position, want)
		}
	})
	t.Run("Add multiple trades and view multiple portfolio endpoints", func(t *testing.T) {
		for i := int64(0); i <= 10; i++ {
			resp, err := addTrade(url, 1, 2, 3.999999999999, i+1)
			if err != nil {
				t.Errorf("error creating trade %s", err)
			}
			if resp.StatusCode != http.StatusOK {
				t.Errorf("error creating trade: %s", err)
			}
		}
		for i := int64(0); i <= 10; i++ {
			position, err := checkPosition(url, 1, i+1)
			if err != nil {
				t.Errorf("error getting current portfolio %s", err)
			}
			want := v1.Position{InstrumentID: 1, Size: 4 + i*2, Timestamp: i + 1}
			if *position != want {
				t.Errorf("got = %v, want %v", position, want)
			}
		}
	})
}

func TestScale(t *testing.T) {
	testPort := 31236
	url := fmt.Sprintf("http://localhost:%d", testPort)

	// TODO: This is a large-scale test. Always skip unless someone opts in.
	// t.Skip()

	t.Run("Add a large number of trades", func(t *testing.T) {
		filename := "/tmp/filestore"
		defer func(name string) {
			err := os.Remove(name)
			if err != nil {
				t.Error(err)
			}
		}(filename)
		// Create the persistent file if it does not exist.
		_, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Fatal(err)
		}

		// Create the filestore
		filestore := &store.FileStore{Filename: filename}

		// startServer creates the server with a cache and uses the filestore.
		// It will time out if the server does not become healthy after a setup period.
		if err := startServer(filestore, 10*time.Second, testPort); err != nil {
			t.Fatal(err)
		}

		// Create a million new records with random values concurrently.
		// Use a channel and worker pool to make the requests happen 100 at a time.
		numberOfTrades := 1_000_000

		// The test will fail if all trades are not processed after a timeout.
		// timeout may have to be tweaked depending on the system and the numberOfTrades.
		timeout := 60 * time.Second

		trades := make(chan int, numberOfTrades)
		results := make(chan int, numberOfTrades)
		for i := int64(0); i <= 100; i++ {
			go func(trades chan int, results chan int) {
				for trade := range trades {
					id := rand.Int63n(10) + 1
					size := rand.Int63n(1000) + 1
					price := float64(rand.Intn(1000)) + 1*rand.Float64()
					timestamp := rand.Int63n(time.Now().Unix()) + 1
					resp, err := addTrade(url, id, size, price, timestamp)
					if err != nil {
						// If there's an error adding a trade tolerate it as it's probably a timer or other flooding issue.
						log.Printf("error creating trade %s", err)
						return
					}
					if resp == nil || resp.StatusCode != http.StatusOK {
						// If there's an error adding a trade tolerate it as it's probably a timout or other flooding issue.
						log.Printf("error creating trade. Response: %v", resp)
						return
					}
					// Put the tradeID in the result queue to keep track of which jobs are finished.
					results <- trade
				}
			}(trades, results)
		}

		// Assign the jobs here by pushing trade jobs into the worker channel.
		start := time.Now()
		for trade := 1; trade <= numberOfTrades; trade++ {
			trades <- trade
		}

		// Set up a timer to enforce the timeout.
		timer := time.After(timeout)

		// Count the number of jobs completed without error.
		var totalCompleted int

	loop:
		for i := 1; i <= numberOfTrades; i++ {
			select {
			case <-results:
				totalCompleted++
			case <-timer:
				t.Errorf("test timed out after %v", timeout)
				break loop
			}
		}
		defer log.Printf("Jobs completed: %v/%v in %s\n", totalCompleted, numberOfTrades, time.Since(start))
	})
}

func checkPosition(url string, instrumentID int64, timestamp int64) (*v1.Position, error) {
	url = fmt.Sprintf("%s/api/v1/position/%d/%d/", url, instrumentID, timestamp)

	client := http.Client{}
	resp, err := client.Get(url)
	if err != nil {
		return &v1.Position{}, fmt.Errorf("error getting current portfolio %s", err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return &v1.Position{}, fmt.Errorf("error reading response body %s", err)
	}
	out := &v1.Position{}
	err = json.Unmarshal(body, out)
	if err != nil {
		return &v1.Position{}, fmt.Errorf("error unmarshalling Position %s", err)
	}
	return out, nil
}

func addTrade(url string, instrumentID int64, size int64, price float64, timestamp int64) (*http.Response, error) {
	url = fmt.Sprintf("%s/api/v1/trade/", url)
	client := http.Client{}
	resp, err := client.Post(url,
		"application/json",
		strings.NewReader(
			fmt.Sprintf("{\"instrument_id\":%d,\"trade_size\":%d,\"trade_price\":%f,\"trade_timestamp\":%d}",
				instrumentID, size, price, timestamp),
		))
	return resp, err
}

// startServer starts a test server with the past filestore. If the server is not responding after a 10 second timeout
// period this function returns an error.
func startServer(filestore *store.FileStore, timeout time.Duration, port int) error {
	go Start(&store.CacheWithPersistentStore{
		Persistent: filestore,
		Cache:      store.InitializeCache(filestore),
	}, port)

	// Wait for the server to be up and running to continue the test. Timeout and return an error if this takes longer
	// than the timeout.
	serverStartTimer := time.After(timeout)
	for {
		select {
		case <-serverStartTimer:
			return fmt.Errorf("server startup timed out after %v", timeout)
		default:
			if _, err := http.Get(fmt.Sprintf("http://localhost:%d/", port)); err != nil {
				continue
			}
			break
		}
		break
	}
	return nil
}
