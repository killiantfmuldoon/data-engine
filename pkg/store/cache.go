package store

import (
	v1 "data-engine/pkg/api/v1"
	"fmt"
	"strconv"
	"sync"
)

// TradeWriter can write a trade to the store.
type TradeWriter interface {
	WriteTrade(v1.Trade) error
}

// PositionReader reads up-to-date position information.
type PositionReader interface {
	ReadPosition(string, string) (*v1.Position, error)
}

// Client can write trades and read positions.
type Client interface {
	TradeWriter
	PositionReader
}

// Cache is a naive cache implementation which does operations on basic data structures. It also functions as a client.
type Cache struct {
	trades    []v1.Trade
	positions map[string]map[string]v1.Position
	sync.RWMutex
}

// InitializeCache reads the state from the persistent store and returns a Client with the most recent good state.
func InitializeCache(persistentStorage Persistent) *Cache {
	// If persistent storage isn't enabled return an empty cache.
	if !persistentStorage.Enabled() {
		return &Cache{
			[]v1.Trade{},
			map[string]map[string]v1.Position{},
			sync.RWMutex{},
		}
	}

	// Fill the Client with data from the persistent store.
	return persistentStorage.ToCache()
}

// WriteTrade adds a Trade to the append-only list of trades.
func (c *Cache) WriteTrade(trade v1.Trade) error {
	c.trades = append(c.trades, trade)
	c.addToPosition(trade)
	return nil
}

// ReadPosition returns the position for the given intrumentID at the given timestamp.
func (c *Cache) ReadPosition(instrumentID string, timestamp string) (*v1.Position, error) {
	c.RWMutex.RLock()
	defer c.RWMutex.RUnlock()
	if v, ok := c.positions[instrumentID][timestamp]; ok {
		return &v, nil
	}
	return nil, fmt.Errorf("position %s not found", instrumentID)
}

// addToPosition updates the position in the map for a given trade.
func (c *Cache) addToPosition(trade v1.Trade) {
	c.RWMutex.Lock()
	defer c.RWMutex.Unlock()
	positionKey := strconv.FormatInt(trade.InstrumentID, 10)
	timestamp := strconv.FormatInt(trade.Timestamp, 10)
	// If there is no positions for this instrument initialize with this trade.
	if _, ok := c.positions[positionKey]; !ok {
		c.positions[positionKey] = map[string]v1.Position{}
		position := v1.Position{
			InstrumentID: trade.InstrumentID,
			Size:         trade.Size,
			Timestamp:    trade.Timestamp,
		}
		c.positions[positionKey][timestamp] = position
		c.positions[positionKey]["current"] = position
		return
	}
	if position, ok := c.positions[positionKey]["current"]; ok {
		// Note: position can go negative here. This is undefined behaviour.
		// As we don't know the semantic meaning of these values we also don't delete the position when it becomes 0.
		position.Size += trade.Size
		position.Timestamp = trade.Timestamp
		c.positions[positionKey][timestamp] = position
		c.positions[positionKey]["current"] = position
	}
}

// CacheWithPersistentStore contains both a Cache and a Persistent store. On writing it writes to both the Cache and the
// Persistent Store. Reading is always done from the Cache.
type CacheWithPersistentStore struct {
	Persistent Persistent
	Cache      Client
}

func (c CacheWithPersistentStore) WriteTrade(trade v1.Trade) error {
	if c.Persistent.Enabled() {
		if err := c.Persistent.WriteTrade(trade); err != nil {
			return err
		}
	}
	if err := c.Cache.WriteTrade(trade); err != nil {
		return err
	}
	return nil
}

// ReadPosition returns the position for the given instrumentID at the given timestamp.
func (c *CacheWithPersistentStore) ReadPosition(instrumentID string, timestamp string) (*v1.Position, error) {
	return c.Cache.ReadPosition(instrumentID, timestamp)
}
