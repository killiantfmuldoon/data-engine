package store

import (
	v1 "data-engine/pkg/api/v1"
	"reflect"
	"strconv"
	"sync"
	"testing"
)

func TestCacheWithPersistentStore_ReadPosition(t *testing.T) {
	type fields struct {
		Persistent Persistent
		Cache      Client
	}
	type args struct {
		instrumentID string
		timestamp    string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *v1.Position
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &CacheWithPersistentStore{
				Persistent: tt.fields.Persistent,
				Cache:      tt.fields.Cache,
			}
			got, err := c.ReadPosition(tt.args.instrumentID, tt.args.timestamp)
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadPosition() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ReadPosition() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCacheWithPersistentStore_WriteTrade(t *testing.T) {
	tests := []struct {
		name    string
		store   CacheWithPersistentStore
		trade   v1.Trade
		wantErr bool
	}{
		{
			name: "successful write to cache and store",
			store: CacheWithPersistentStore{
				Persistent: &fakePersistentStore{trades: []v1.Trade{}},
				Cache:      InitializeCache(&fakePersistentStore{}),
			},
			trade:   v1.Trade{InstrumentID: 1, Size: 2, Price: 3, Timestamp: 4},
			wantErr: false,
		},
		{
			name: "skip persistent write if disabled",
			store: CacheWithPersistentStore{
				Persistent: &fakePersistentStore{nil},
				Cache:      InitializeCache(&fakePersistentStore{}),
			},
			trade:   v1.Trade{InstrumentID: 1, Size: 2, Price: 3, Timestamp: 4},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.store.WriteTrade(tt.trade); (err != nil) != tt.wantErr {
				t.Errorf("WriteTrade() error = %v, wantErr %v", err, tt.wantErr)
			}
			// Ensure the trade is written to the Persistent store if it's enabled.
			peristent := tt.store.Persistent.(*fakePersistentStore)
			if tt.store.Persistent.Enabled() && peristent.trades[0] != tt.trade {
				t.Errorf("WriteTrade() got = %v, want = %v", peristent.trades[0], tt.trade)
			}

			// Ensure the trade is written to the Cache.
			cache := tt.store.Cache.(*Cache)
			if cache.trades[0] != tt.trade {
				t.Errorf("WriteTrade() got = %v, want = %v", cache.trades[0], tt.trade)
			}
		})
	}
}

func TestCache_ReadPosition(t *testing.T) {
	type fields struct {
		trades    []v1.Trade
		positions map[string]map[string]v1.Position
	}
	type args struct {
		instrumentID string
		timestamp    string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *v1.Position
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Cache{
				trades:    tt.fields.trades,
				positions: tt.fields.positions,
			}
			got, err := c.ReadPosition(tt.args.instrumentID, tt.args.timestamp)
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadPosition() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ReadPosition() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCache_WriteTrade(t *testing.T) {
	type fields struct {
		trades    []v1.Trade
		positions map[string]map[string]v1.Position
	}
	type args struct {
		trade v1.Trade
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Cache{
				trades:    tt.fields.trades,
				positions: tt.fields.positions,
			}
			if err := c.WriteTrade(tt.args.trade); (err != nil) != tt.wantErr {
				t.Errorf("WriteTrade() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCache_addToPosition(t *testing.T) {

	tests := []struct {
		name  string
		cache *Cache
		trade v1.Trade
		want  *v1.Position
	}{
		{
			name:  "successfully add first trade to position",
			cache: &Cache{[]v1.Trade{}, map[string]map[string]v1.Position{}, sync.RWMutex{}},
			trade: v1.Trade{InstrumentID: 1, Size: 2, Price: 3, Timestamp: 4},
			want:  &v1.Position{InstrumentID: 1, Size: 2, Timestamp: 4},
		},
		{
			name: "successfully add trade to existing position",
			cache: &Cache{[]v1.Trade{}, map[string]map[string]v1.Position{
				"1": {
					"4":       v1.Position{InstrumentID: 1, Size: 2, Timestamp: 4},
					"current": v1.Position{InstrumentID: 1, Size: 2, Timestamp: 4},
				},
			},
				sync.RWMutex{},
			},
			trade: v1.Trade{InstrumentID: 1, Size: 2, Price: 3, Timestamp: 5},
			want:  &v1.Position{InstrumentID: 1, Size: 4, Timestamp: 5},
		},
		{
			name: "successfully subtract trade from position",
			cache: &Cache{[]v1.Trade{}, map[string]map[string]v1.Position{
				"1": {
					"4":       v1.Position{InstrumentID: 1, Size: 2, Timestamp: 4},
					"current": v1.Position{InstrumentID: 1, Size: 2, Timestamp: 4},
				},
			},
				sync.RWMutex{},
			},
			trade: v1.Trade{InstrumentID: 1, Size: -2, Price: 3, Timestamp: 5},
			want:  &v1.Position{InstrumentID: 1, Size: 0, Timestamp: 5},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.cache.addToPosition(tt.trade)
			got, _ := tt.cache.ReadPosition(
				strconv.FormatInt(tt.trade.InstrumentID, 10),
				strconv.FormatInt(tt.trade.Timestamp, 10))

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("addToPosition() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestInitializeCache(t *testing.T) {
	tests := []struct {
		name       string
		persistent Persistent
		want       *Cache
	}{
		{
			name: "succeed at initializing cache from fakePersistentStore",
			persistent: &fakePersistentStore{trades: []v1.Trade{
				{InstrumentID: 1, Size: 2, Price: 3, Timestamp: 4},
			}},
			want: &Cache{
				trades: []v1.Trade{
					{InstrumentID: 1, Size: 2, Price: 3, Timestamp: 4},
				},
				positions: map[string]map[string]v1.Position{},
			},
		},
		{
			name:       "succeed at initializing empty cache with Cache disabled",
			persistent: &fakePersistentStore{trades: nil},
			want: &Cache{
				trades:    []v1.Trade{},
				positions: map[string]map[string]v1.Position{},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InitializeCache(tt.persistent); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InitializeCache() = %v, want %v", got, tt.want)
			}
		})
	}
}

type fakePersistentStore struct {
	trades []v1.Trade
}

func (f *fakePersistentStore) Enabled() bool {
	return f.trades != nil
}

func (f *fakePersistentStore) ToCache() *Cache {
	return &Cache{
		trades:    f.trades,
		positions: map[string]map[string]v1.Position{},
	}
}

func (f *fakePersistentStore) WriteTrade(trade v1.Trade) error {
	f.trades = append(f.trades, trade)
	return nil
}
