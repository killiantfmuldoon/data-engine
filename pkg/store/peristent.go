package store

import (
	"bufio"
	v1 "data-engine/pkg/api/v1"
	"encoding/json"
	"log"
	"os"
	"sync"
)

type Persistent interface {
	Enabled() bool
	ToCache() *Cache
	TradeWriter
}

// FileStore points to out-of-memory storage for trade information.
type FileStore struct {
	Filename string
}

// Enabled returns true if the persistent store is configured.
func (p *FileStore) Enabled() bool {
	return p.Filename != ""
}

// ToCache fills a cache with information from the persistent store.
func (p *FileStore) ToCache() *Cache {
	cache := &Cache{
		[]v1.Trade{},
		map[string]map[string]v1.Position{},
		sync.RWMutex{},
	}

	// Open the persistent storage file, read each line and add to the cache as a trade.
	file, err := os.Open(p.Filename)
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(file)

	trade := &v1.Trade{}
	// Here we decode the request and write it to the datastore.
	for scanner.Scan() {
		if err := json.Unmarshal(scanner.Bytes(), trade); err != nil {
			log.Fatal(err)
		}
		if err := cache.WriteTrade(*trade); err != nil {
			log.Fatal(err)
		}
		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}
	}
	if err = file.Close(); err != nil {
		log.Fatal(err)
	}
	return cache
}

// WriteTrade appends a trade to the persistent file.
func (p *FileStore) WriteTrade(trade v1.Trade) error {
	f, err := os.OpenFile(p.Filename, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		return err
	}
	tradeData, err := trade.ToJSONString()
	if err != nil {
		return err
	}

	if _, err = f.WriteString(tradeData + "\n"); err != nil {
		return err
	}
	return f.Close()
}
