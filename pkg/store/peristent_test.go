package store

import (
	v1 "data-engine/pkg/api/v1"
	"log"
	"os"
	"reflect"
	"testing"
)

func TestFileStore_Enabled(t *testing.T) {
	tests := []struct {
		name     string
		Filename string
		want     bool
	}{
		{
			name:     "Enabled true if fileName exists",
			Filename: "exists",
			want:     true,
		},
		{
			name:     "Enabled false if fileName does not exist",
			Filename: "",
			want:     false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &FileStore{
				Filename: tt.Filename,
			}
			if got := p.Enabled(); got != tt.want {
				t.Errorf("Enabled() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Note this test creates a file in the filesystem under /tmp/filestore.
// This may not work on Windows or other OS depending on permissions.
func TestFileStore_ToCache(t *testing.T) {
	t.Run("test cache initialization", func(t *testing.T) {
		f, err := os.OpenFile("/tmp/filestore", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Fatal(err)
		}

		// defer cleanup of the file
		defer os.Remove("/tmp/filestore")

		_, err = f.WriteString("{\"instrument_id\":1,\"trade_size\":2,\"trade_price\":3,\"trade_timestamp\":4}\n{\"instrument_id\":233,\"trade_size\":150,\"trade_price\":7,\"trade_timestamp\":1650896181}")
		if err != nil {
			log.Fatal(err)
		}
		cache := &Cache{
			trades: []v1.Trade{
				{InstrumentID: 1, Size: 2, Price: 3, Timestamp: 4},
				{InstrumentID: 233, Size: 150, Price: 7, Timestamp: 1650896181},
			},
			positions: map[string]map[string]v1.Position{
				"1": {
					"4":       v1.Position{InstrumentID: 1, Size: 2, Timestamp: 4},
					"current": v1.Position{InstrumentID: 1, Size: 2, Timestamp: 4},
				},
				"233": {
					"1650896181": v1.Position{InstrumentID: 233, Size: 150, Timestamp: 1650896181},
					"current":    v1.Position{InstrumentID: 233, Size: 150, Timestamp: 1650896181},
				},
			},
		}
		p := &FileStore{
			Filename: "/tmp/filestore",
		}

		if got := p.ToCache(); !reflect.DeepEqual(got.trades, cache.trades) {
			t.Errorf("ToCache() = %v, want %v", got.trades, cache.trades)
		}
		if got := p.ToCache(); !reflect.DeepEqual(got.positions, cache.positions) {
			t.Errorf("ToCache() = %v, want %v", got.positions, cache.positions)
		}
	})
}

func TestFileStore_WriteTrade(t *testing.T) {
	t.Run("write trade to file", func(t *testing.T) {
		_, err := os.OpenFile("/tmp/filestore", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Fatal(err)
		}
		// defer cleanup of the file
		defer os.Remove("/tmp/filestore")

		if err != nil {
			log.Fatal(err)
		}
		trade := v1.Trade{InstrumentID: 1, Size: 2, Price: 3, Timestamp: 4}
		p := &FileStore{
			Filename: "/tmp/filestore",
		}
		if err := p.WriteTrade(trade); err != nil {
			t.Errorf("WriteTrade() error = %v", err)
		}
		contents, err := os.ReadFile("/tmp/filestore")
		if err != nil {
			t.Errorf("File read error = %v", err)
		}

		if got := string(contents); !reflect.DeepEqual(got, "{\"instrument_id\":1,\"trade_size\":2,\"trade_price\":3,\"trade_timestamp\":4}\n") {
			t.Errorf("got = %v, want %v", got, "{\"instrument_id\":1,\"trade_size\":2,\"trade_price\":3,\"trade_timestamp\":4}\n")
		}
	})

}
